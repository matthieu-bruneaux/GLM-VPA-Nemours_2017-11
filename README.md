# Welcome to the repository for the GLM course in Nemours (November 2017)

This is a wiki-only repository. Please visit the
[wiki](https://gitlab.com/matthieu-bruneaux/GLM-VPA-Nemours_2017-11/wikis/home)!
